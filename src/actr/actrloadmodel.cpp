#include <QtWidgets>
#include <QDebug>
#include "actrenvironment.h"
#include "actrmanager.h"

static QString modelHandler = "load_model";
static QString returnHandler = "load_model_return";
static QString setupCommand =
"create list-handler ${HANDLER} ${RETURN} (lambda (x)) ()";

static const char *loadCommand =
"update ${HANDLER}                                              \
    (lambda (x)                                                 \
        (declare (ignore x))                                    \
        (progn                                                  \
            (set-environment-busy)                              \
            (let ((result (safe-load \"${FILENAME}\" nil)))     \
                (set-environment-free)                          \
                (format t (if (= 1 (first result))              \
                    \"Modelo carregado com sucesso!\"           \
                    \"Falha ao carregar o modelo\"))            \
                result))                                        \
            (set-environment-free))";

void ActrEnvironment::setupLoadModel()
{
    QString cmd = setupCommand.replace("${HANDLER}", modelHandler)
                              .replace("${RETURN}", returnHandler);
    sendEnvironmentCmd(cmd);

    m_dispatcher[returnHandler] = [&](QString type, QString rest, QString args) {
        qDebug() << QString("Tipo:").append(type).append(", Valor:").append(rest);
        //ui->textEdit->append(QString("Tipo:").append(type).append(", Valor:").append(rest));
    };
}

void ActrEnvironment::loadModel()
{
    if (isValidConnection()) {
        QString name = QFileDialog::getOpenFileName(m_manager, "Modelo", "", "*.lisp");
        if (!name.isEmpty()) {
            QString cmd = QString(loadCommand)
                    .replace("${FILENAME}", name)
                    .replace("${HANDLER}", m_handlers[modelHandler])
                    .replace(QRegExp("\\s+")," ");
            sendEnvironmentCmd(cmd);
        }
    }
}

void ActrEnvironment::loadModel(const QString &name)
{
    if (isValidConnection()) {
        if (!name.isEmpty()) {
            QString cmd = QString(loadCommand)
                .replace("${FILENAME}", name)
                .replace("${HANDLER}", m_handlers[modelHandler])
                .replace(QRegExp("\\s+")," ");
            sendEnvironmentCmd(cmd);
        }
    }
}
