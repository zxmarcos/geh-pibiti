#include <QtWidgets>
#include <QDebug>
#include "../devices/virtualmouse.h"
#include "actrenvironment.h"
#include "actrmanager.h"

static QString modelHandler = ".env_window";
static QString returnHandler = ".env_window";
static QString setupCommand =
"create env-window-handler ${HANDLER} ${RETURN}\
(lambda (x) \
    (declare (ignore x))    \
    (setf (environment-control-use-env-windows *environment-control*) t)\
    (list 'ignore)) ()";

void ActrEnvironment::setupEnvironmentWindow()
{
    QString cmd = setupCommand.replace("${HANDLER}", modelHandler)
                              .replace("${RETURN}", returnHandler);
    sendEnvironmentCmd(cmd);

    m_dispatcher[returnHandler] = [&](QString type, QString rest, QString args) {
        processEnvironmentWindow(rest, args);
    };
}


void ActrEnvironment::processEnvironmentWindow(QString cmd, QString args)
{
    qDebug() << cmd << args;
    if (cmd == QString("cursor")) {
        QRegExp mat("\\s*(\".*\")\\s+(\\d+)\\s+(\\d+).*$");
        if (mat.exactMatch(args)) {
            int x = mat.cap(2).toInt();
            int y = mat.cap(3).toInt();
            qDebug() << "Mover cursor" << x << y;
            VirtualMouse::get()->move(x, y);
        }
    }
}
