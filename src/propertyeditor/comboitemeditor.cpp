#include <QtWidgets>
#include "comboitemeditor.h"

ComboItemEditor::ComboItemEditor(QWidget* parent) :
    QDialog(parent)
{
    setModal(true);

    QVBoxLayout *layout = new QVBoxLayout();

    m_list = new QListView(this);
    m_close = new QPushButton(tr("Fechar"));
    m_insert = new QPushButton(tr("Inserir"));
    m_remove = new QPushButton(tr("Remover"));

    QHBoxLayout *bottom = new QHBoxLayout();
    bottom->addWidget(m_insert);
    bottom->addWidget(m_remove);
    bottom->addWidget(m_close);

    layout->addWidget(m_list);
    layout->addLayout(bottom);

    connect(m_close, SIGNAL(clicked()), this, SLOT(accept()));
    connect(m_insert, SIGNAL(clicked()), this, SLOT(insert()));
    connect(m_remove, SIGNAL(clicked()), this, SLOT(remove()));

    setLayout(layout);
}

void ComboItemEditor::setComboBox(QComboBox *combo)
{
    m_list->setModel(combo->model());
}

void ComboItemEditor::insert()
{
    QAbstractItemModel *model = m_list->model();

    int row = model->rowCount();

    model->insertRow(row);
    QModelIndex index = model->index(row, 0);

    model->setData(index, "Novo");

    m_list->setCurrentIndex(index);
    m_list->edit(index);
}

void ComboItemEditor::remove()
{
    QAbstractItemModel *model = m_list->model();
    QModelIndex index = m_list->currentIndex();
    model->removeRow(index.row());
}
