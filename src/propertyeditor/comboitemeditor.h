#ifndef COMBOITEMEDITOR_H
#define COMBOITEMEDITOR_H

#include <QDialog>

class QListView;
class QAbstractItemModel;
class QPushButton;
class QComboBox;

class ComboItemEditor : public QDialog
{
    Q_OBJECT
public:
    explicit ComboItemEditor(QWidget* parent = 0);

    void setComboBox(QComboBox* combo);
signals:

public slots:
    void insert();
    void remove();
private:
    QListView* m_list;
    QPushButton* m_insert;
    QPushButton* m_remove;
    QPushButton* m_close;
};

#endif // COMBOITEMEDITOR_H
