#ifndef QBOOLEDIT_H
#define QBOOLEDIT_H

#include <QComboBox>

class QBoolEdit : public QComboBox
{
    Q_OBJECT
    Q_PROPERTY(bool m_value READ value WRITE setValue USER true)
public:
    explicit QBoolEdit(QWidget *parent = 0);

    bool value() const;
public slots:
    void setValue(bool value);
};

#endif // QBOOLEDIT_H
