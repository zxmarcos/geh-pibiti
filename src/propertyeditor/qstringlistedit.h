#ifndef QSTRINGLISTEDIT_H
#define QSTRINGLISTEDIT_H

#include <QDialog>
#include <QTextEdit>
#include <QPushButton>
#include <QLineEdit>

class QStringListEditDialog;

class QStringListEdit : public QPushButton
{
    Q_OBJECT
    Q_PROPERTY(QStringList m_list READ list WRITE setList USER true)
public:
    explicit QStringListEdit(QWidget *parent = 0);
    QStringList list() const;

public slots:
    void setList(const QStringList &list);
private:
    QStringList m_list;
    QStringListEditDialog *m_editor;
};


class QStringListEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit QStringListEditDialog(QWidget *parent = 0);

signals:

public slots:
private:
    QTextEdit *m_textEdit;
    QPushButton *m_ok;
    QPushButton *m_cancel;
};

#endif // QSTRINGLISTEDIT_H
