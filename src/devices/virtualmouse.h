#ifndef VIRTUALMOUSE_H
#define VIRTUALMOUSE_H

#include <QWidget>

class VirtualMouse : public QWidget
{
    Q_OBJECT
public:
    explicit VirtualMouse(QWidget *parent = 0);

signals:

public slots:
protected:
    virtual void paintEvent(QPaintEvent *event);
private:
    QPixmap m_arrow;
    static VirtualMouse *sm_instance;
public:
    static VirtualMouse *get() {
        return sm_instance;
    }
};

#endif // VIRTUALMOUSE_H
