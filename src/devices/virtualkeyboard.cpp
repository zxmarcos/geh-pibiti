// =============================================================================
// Copyright (C) 2015    Marcos Medeiros
//
// This file is part of "SGEH - Sistema Gerador de Erro Humano"
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// =============================================================================
#include <QtWidgets>
#include "virtualkeyboard.h"

using namespace detail;

VirtualKeyboard::VirtualKeyboard(QWidget *parent) :
    QWidget(parent)
{
    const int keysPerRow = 23;
    setFixedWidth(23 * vkWidth + keysPerRow - 1);
    setFixedHeight(7 * vkHeight);

    QVBoxLayout *main = new QVBoxLayout;
    main->setMargin(3);
    main->setSpacing(1);

    QHBoxLayout *layout;

    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(new VirtualKey(0, 0, "ESC"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "F1"));
    layout->addWidget(new VirtualKey(0, 0, "F2"));
    layout->addWidget(new VirtualKey(0, 0, "F3"));
    layout->addWidget(new VirtualKey(0, 0, "F4"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "F5"));
    layout->addWidget(new VirtualKey(0, 0, "F6"));
    layout->addWidget(new VirtualKey(0, 0, "F7"));
    layout->addWidget(new VirtualKey(0, 0, "F8"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "F9"));
    layout->addWidget(new VirtualKey(0, 0, "F10"));
    layout->addWidget(new VirtualKey(0, 0, "F11"));
    layout->addWidget(new VirtualKey(0, 0, "F12"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "F13"));
    layout->addWidget(new VirtualKey(0, 0, "F14"));
    layout->addWidget(new VirtualKey(0, 0, "F15"));
    layout->addWidget(new SpacerKey(5));
    main->addLayout(layout);

    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(new VirtualKey(0, 0, "`"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "1"));
    layout->addWidget(new VirtualKey(0, 0, "2"));
    layout->addWidget(new VirtualKey(0, 0, "3"));
    layout->addWidget(new VirtualKey(0, 0, "4"));
    layout->addWidget(new VirtualKey(0, 0, "5"));
    layout->addWidget(new VirtualKey(0, 0, "6"));
    layout->addWidget(new VirtualKey(0, 0, "7"));
    layout->addWidget(new VirtualKey(0, 0, "8"));
    layout->addWidget(new VirtualKey(0, 0, "9"));
    layout->addWidget(new VirtualKey(0, 0, "0"));
    layout->addWidget(new VirtualKey(0, 0, "-"));
    layout->addWidget(new VirtualKey(0, 0, "="));
    layout->addWidget(new VirtualKey(0, 0, "DEL"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "HELP"));
    layout->addWidget(new VirtualKey(0, 0, "HOME"));
    layout->addWidget(new VirtualKey(0, 0, "PGUP"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "CLR"));
    layout->addWidget(new VirtualKey(0, 0, "="));
    layout->addWidget(new VirtualKey(0, 0, "/"));
    layout->addWidget(new VirtualKey(0, 0, "*"));
    main->addLayout(layout);

    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(new VirtualKey(0, 0, "Tab", 2));
    layout->addWidget(new VirtualKey(0, 0, "Q"));
    layout->addWidget(new VirtualKey(0, 0, "W"));
    layout->addWidget(new VirtualKey(0, 0, "E"));
    layout->addWidget(new VirtualKey(0, 0, "R"));
    layout->addWidget(new VirtualKey(0, 0, "T"));
    layout->addWidget(new VirtualKey(0, 0, "Y"));
    layout->addWidget(new VirtualKey(0, 0, "U"));
    layout->addWidget(new VirtualKey(0, 0, "I"));
    layout->addWidget(new VirtualKey(0, 0, "O"));
    layout->addWidget(new VirtualKey(0, 0, "P"));
    layout->addWidget(new VirtualKey(0, 0, "["));
    layout->addWidget(new VirtualKey(0, 0, "]"));
    layout->addWidget(new VirtualKey(0, 0, "\\"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "FDEL"));
    layout->addWidget(new VirtualKey(0, 0, "END"));
    layout->addWidget(new VirtualKey(0, 0, "PGDN"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "7"));
    layout->addWidget(new VirtualKey(0, 0, "8"));
    layout->addWidget(new VirtualKey(0, 0, "9"));
    layout->addWidget(new VirtualKey(0, 0, "-"));
    main->addLayout(layout);

    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(new VirtualKey(0, 0, "Cps Lk", 2));
    layout->addWidget(new VirtualKey(0, 0, "A"));
    layout->addWidget(new VirtualKey(0, 0, "S"));
    layout->addWidget(new VirtualKey(0, 0, "D"));
    layout->addWidget(new VirtualKey(0, 0, "F"));
    layout->addWidget(new VirtualKey(0, 0, "G"));
    layout->addWidget(new VirtualKey(0, 0, "H"));
    layout->addWidget(new VirtualKey(0, 0, "J"));
    layout->addWidget(new VirtualKey(0, 0, "K"));
    layout->addWidget(new VirtualKey(0, 0, "L"));
    layout->addWidget(new VirtualKey(0, 0, ";"));
    layout->addWidget(new VirtualKey(0, 0, "'"));
    layout->addWidget(new VirtualKey(0, 0, "Ret", 2));
    layout->addWidget(new SpacerKey(5));
    layout->addWidget(new VirtualKey(0, 0, "4"));
    layout->addWidget(new VirtualKey(0, 0, "5"));
    layout->addWidget(new VirtualKey(0, 0, "6"));
    layout->addWidget(new VirtualKey(0, 0, "+"));
    main->addLayout(layout);

    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(new VirtualKey(0, 0, "Shift", 2));
    layout->addWidget(new VirtualKey(0, 0, "Z"));
    layout->addWidget(new VirtualKey(0, 0, "X"));
    layout->addWidget(new VirtualKey(0, 0, "C"));
    layout->addWidget(new VirtualKey(0, 0, "V"));
    layout->addWidget(new VirtualKey(0, 0, "B"));
    layout->addWidget(new VirtualKey(0, 0, "N"));
    layout->addWidget(new VirtualKey(0, 0, "M"));
    layout->addWidget(new VirtualKey(0, 0, ","));
    layout->addWidget(new VirtualKey(0, 0, "."));
    layout->addWidget(new VirtualKey(0, 0, "Shift", 2));;
    layout->addWidget(new SpacerKey(4));
    layout->addWidget(new VirtualKey(0, 0, "UP"));
    layout->addWidget(new SpacerKey(2));
    layout->addWidget(new VirtualKey(0, 0, "1"));
    layout->addWidget(new VirtualKey(0, 0, "2"));
    layout->addWidget(new VirtualKey(0, 0, "3"));
    layout->addWidget(new SpacerKey());
    main->addLayout(layout);

    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(new VirtualKey(0, 0, "Ctrl", 2));
    layout->addWidget(new VirtualKey(0, 0, "Opt"));
    layout->addWidget(new VirtualKey(0, 0, "Cmd"));
    layout->addWidget(new VirtualKey(0, 0, "Space", 8));
    layout->addWidget(new VirtualKey(0, 0, "Cmd"));
    layout->addWidget(new VirtualKey(0, 0, "Opt"));
    layout->addWidget(new VirtualKey(0, 0, "Ctrl"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, "LEFT"));
    layout->addWidget(new VirtualKey(0, 0, "DOWN"));
    layout->addWidget(new VirtualKey(0, 0, "RIGHT"));
    layout->addWidget(new SpacerKey());
    layout->addWidget(new VirtualKey(0, 0, " "));
    layout->addWidget(new VirtualKey(0, 0, "0"));
    layout->addWidget(new VirtualKey(0, 0, "."));
    layout->addWidget(new SpacerKey());
    main->addLayout(layout);


    setLayout(main);
}

VirtualKeyboard::~VirtualKeyboard()
{
}

void VirtualKeyboard::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setClipRect(event->rect());

    p.fillRect(rect(), Qt::darkGray);
    p.setPen(Qt::black);
    p.drawRect(rect().adjusted(0,0,-1,-1));
}

namespace detail {

VirtualKey::VirtualKey(int row, int column, QString name, int nslots, QWidget *parent) :
    QWidget(parent), m_row(row), m_column(column), m_char(name)
{
    m_state = NORMAL;
    setFixedHeight(vkHeight);
    setFixedWidth(nslots*vkWidth+nslots-1);
}

void VirtualKey::release()
{
    m_state = NORMAL;
    update();
}

void VirtualKey::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setClipRect(event->rect());

    if (m_state == NORMAL) {
        p.setPen(Qt::black);
        p.setBrush(Qt::lightGray);
    } else {
        p.setPen(Qt::lightGray);
        p.setBrush(Qt::black);
    }
    QFont f;
    switch (m_char.size()) {
    case 1:
        f.setPointSize(9);
        break;
    case 2:
    case 3:
        f.setPixelSize(10);
        break;
    default:
        f.setPixelSize(7);
        break;
    }
    p.setFont(f);
    p.drawRoundRect(rect().adjusted(0, 0, -1, -1), 0, 0);
    p.drawText(rect(), Qt::AlignHCenter | Qt::AlignVCenter, m_char);
}

void VirtualKey::mousePressEvent(QMouseEvent *event)
{
    m_state = PRESSED;
    update();
    QTimer::singleShot(200, this, SLOT(release()));
}

void VirtualKey::mouseReleaseEvent(QMouseEvent *event)
{
}


}
