#include <QtWidgets>
#include "virtualmouse.h"

VirtualMouse *VirtualMouse::sm_instance = nullptr;

VirtualMouse::VirtualMouse(QWidget *parent) :
    QWidget(parent)
{
    sm_instance = this;
    setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents);
    m_arrow = QPixmap(":/images/mouse.png");
    setFixedSize(m_arrow.width(), m_arrow.height());
}

void VirtualMouse::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    //p.fillRect(rect(), QColor(10, 80, 255, 255));
    p.drawPixmap(m_arrow.rect(), m_arrow);
}

