#ifndef VIRTUALDESKTOP_H
#define VIRTUALDESKTOP_H

#include <QWidget>

class VirtualDesktop : public QWidget
{
    Q_OBJECT
public:
    explicit VirtualDesktop(QWidget *parent = 0);

signals:

public slots:
protected:
    virtual bool eventFilter(QObject *obj, QEvent *event);
private:
    static VirtualDesktop *sm_instance;
public:
    static VirtualDesktop *get() {
        return sm_instance;
    }
};

#endif // VIRTUALDESKTOP_H
