#include <QtWidgets>
#include "virtualdesktop.h"

VirtualDesktop *VirtualDesktop::sm_instance = nullptr;

VirtualDesktop::VirtualDesktop(QWidget *parent) :
    QWidget(parent)
{
    sm_instance = this;
    setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents);
}

bool VirtualDesktop::eventFilter(QObject *obj, QEvent *event)
{
    if (!obj->isWidgetType())
        return false;

    QWidget *w = static_cast<QWidget*>(obj);

    if (event->type() == QEvent::Resize) {
        if (w == parent())
            setGeometry(w->geometry());
    }
    return false;

}

