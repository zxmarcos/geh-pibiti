// =============================================================================
// Copyright (C) 2015    Marcos Medeiros
//
// This file is part of "SGEH - Sistema Gerador de Erro Humano"
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// =============================================================================
#include "lispemitter.h"

LispEmitter::LispEmitter()
{
}

void LispEmitter::begin()
{
    m_builder.append('(');
}

void LispEmitter::qbegin()
{
    m_builder.append("'(");
}

void LispEmitter::end()
{
    m_builder.append(')');
}

void LispEmitter::cat(const QString &val)
{
    m_builder.append(val);
    m_builder.append(' ');
}

QString LispEmitter::result()
{
    return m_builder;
}
