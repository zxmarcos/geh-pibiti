// =============================================================================
// Copyright (C) 2015    Marcos Medeiros
//
// This file is part of "SGEH - Sistema Gerador de Erro Humano"
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// =============================================================================
#include "modelcompiler.h"
#include "../designersurface/designersurface.h"
#include "lispemitter.h"
#include <QDebug>
#include <QFile>

ModelCompiler::ModelCompiler()
{
}

void ModelCompiler::compile(const TaskSequence &seq)
{
    m_result.clear();
    m_result.reserve(1024*64);

    m_currentStep = 0;
    emitProlog();

    foreach (SequenceNode node, seq) {
        switch (node.action) {
        case SequenceNode::FILL:
            emitFill(node);
            break;
        case SequenceNode::PRESS:
            emitPress(node);
            break;
        default:
            qDebug() << "ModelCompiler: Operator not implemented";
            break;
        }
    }

    emitEpilog();
    m_result = m_result.replace("\t", "  ");
}

QString ModelCompiler::result()
{
    return m_result;
}

void ModelCompiler::emitProlog()
{
    {
        QFile file(":/lisp/geh-device.lisp");
        file.open(QFile::ReadOnly);
        m_result.append(file.readAll());
    }

    m_result.append(R"(
(defun geh-start-task()
    (let* ((window (open-exp-window "geh-window")))
        (add-items-to-exp-window
    )");

     emitForm();
    m_result.append(R"(
        )
        (install-device window)
        (proc-display)
        (print-visicon)
        (run 1000)))
    )");

    m_result.append(
    R"(
(define-model geh-tarefa

(chunk-type passo ordem tipo estado tecla posicao widget)

(sgp :v t :needs-mouse t :show-focus t :trace-detail medium)
    )");
    emitDM();

}

void ModelCompiler::emitForm()
{
    QList<QWidget*> widgets = DesignerSurface::get()->formWidgets();

    if (widgets.empty()) {
        m_result.append("nil");
        return;
    }
    foreach (QWidget *w, widgets) {
        m_result.append(QString("(make-instance 'geh-widget :x-pos %1 :y-pos %2 :width %3 :height %4)\n")
                        .arg(w->x()).arg(w->y()).arg(w->width()).arg(w->height()));
    }
}

void ModelCompiler::emitFormLocs()
{
    QList<QWidget*> widgets = DesignerSurface::get()->formWidgets();

    foreach (QWidget *w, widgets) {
        m_result.append(
            QString("\t(%1 isa visual-location screen-x %2 screen-y %3)\n")
                        .arg(w->objectName()).arg(w->x()).arg(w->y()));
    }
}

void ModelCompiler::emitDM()
{
    m_result.append("(add-dm\n");
    emitFormLocs();
    m_result.append("\t(meta isa passo ordem 0))\n");
}

void ModelCompiler::emitEpilog()
{
//    emitGehCommon();
    m_result.append("\n(goal-focus meta)\n)\n");
}

void ModelCompiler::emitFill(SequenceNode& node)
{
    emitLookAt(DesignerSurface::get()->findChild<QWidget*>(node.object));
    emitPress(node);
    for (int i = 0; i < node.arg.size(); i++) {
        // Não teclamos o espaço
        if (node.arg.at(i) == ' ')
            continue;
        m_result.append(QString("(P %0\n").arg(generateStepPName()));
        m_result.append(QString(
        R"(
            =goal>
                isa passo
                ordem %0
            ?manual>
                state free
        ==>
            =goal>
                ordem %1
            +manual>
                isa press-key
                key "%3"
        )"
        ).arg(m_currentStep).arg(m_currentStep+1).arg(node.arg.at(i)));
        m_result.append(")\n");
        m_currentStep++;
    }
}

void ModelCompiler::emitPress(SequenceNode &node)
{
    emitHandToMouse();

    // Move o cursor
    m_result.append(QString("(P %0\n").arg(generateStepPName()));
    m_result.append(QString(
    R"(
        =goal>
            isa passo
            ordem %0
        ?manual>
            state free
    ==>
        =goal>
            ordem %1
        +manual>
            isa move-cursor
            loc %2
    )"
    ).arg(m_currentStep).arg(m_currentStep+1).arg(node.object));
    m_result.append(")\n");
    m_currentStep++;

    // Realiza o click
    m_result.append(QString("(P %0\n").arg(generateStepPName()));
    m_result.append(QString(
    R"(
        =goal>
            isa passo
            ordem %0
        ?manual>
            state free
    ==>
        =goal>
            ordem %1
        +manual>
            isa click-mouse
    )"
    ).arg(m_currentStep).arg(m_currentStep+1));
    m_result.append(")\n");
    m_currentStep++;
}

void ModelCompiler::emitLookAt(QWidget *widget)
{
    m_result.append(QString("(P %0\n").arg(generateStepPName()));
    m_result.append(QString(
    R"(
        =goal>
            isa passo
            ordem %0
        ?visual>
            state free
    ==>
        =goal>
            ordem %1
        +visual>
            isa move-attention
            screen-pos %2
    )"
    ).arg(m_currentStep).arg(m_currentStep+1).arg(widget->objectName()));
    m_result.append(")\n");
    m_currentStep++;
}

void ModelCompiler::emitHandToMouse()
{
    // Coloca a mão no mouse
    m_result.append(QString("(P %0\n").arg(generateStepPName()));
    m_result.append(QString(
    R"(
        =goal>
            isa passo
            ordem %0
        ?manual>
            state free
    ==>
        =goal>
            ordem %1
        +manual>
            isa hand-to-mouse
    )"
    ).arg(m_currentStep).arg(m_currentStep+1));
    m_result.append(")\n");
    m_currentStep++;
}

void ModelCompiler::emitGehDevice()
{
}

void ModelCompiler::emitGehCommon()
{
}

void ModelCompiler::emitStartTask()
{
}

void ModelCompiler::emitVisLocsChunks()
{
}

void ModelCompiler::emitVisObjsChunks()
{
}

QString ModelCompiler::generateStepPName()
{
    return QString("P-%1").arg(m_currentStep);
}
